``` xml-dtd
Class Hierarchy
java.lang.Object
java.text.Annotation
java.text.AttributedCharacterIterator.Attribute (implements java.io.Serializable)
java.text.Format.Field
java.text.DateFormat.Field
java.text.MessageFormat.Field
java.text.NumberFormat.Field
java.text.AttributedString
java.text.Bidi
java.text.BreakIterator (implements java.lang.Cloneable)
java.text.CollationElementIterator
java.text.CollationKey (implements java.lang.Comparable<T>)
java.text.Collator (implements java.lang.Cloneable, java.util.Comparator<T>)
java.text.RuleBasedCollator
java.text.DateFormatSymbols (implements java.lang.Cloneable, java.io.Serializable)
java.text.DecimalFormatSymbols (implements java.lang.Cloneable, java.io.Serializable)
java.text.FieldPosition
java.text.Format (implements java.lang.Cloneable, java.io.Serializable)
java.text.DateFormat
java.text.SimpleDateFormat
java.text.MessageFormat
java.text.NumberFormat
java.text.ChoiceFormat
java.text.CompactNumberFormat
java.text.DecimalFormat
java.text.Normalizer
java.text.ParsePosition
java.text.StringCharacterIterator (implements java.text.CharacterIterator)
java.lang.Throwable (implements java.io.Serializable)
java.lang.Exception
java.text.ParseException

Interface Hierarchy
java.lang.Cloneable
java.text.CharacterIterator
java.text.AttributedCharacterIterator
Enum Class Hierarchy
java.lang.Object
java.lang.Enum<E> (implements java.lang.Comparable<T>, java.lang.constant.Constable, java.io.Serializable)
java.text.Normalizer.Form
java.text.NumberFormat.Style
```

> https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/text/package-tree.html

