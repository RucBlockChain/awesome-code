package featuretest.extend;

public interface LifeCycleOwner {
    public String getLifeCycle();
}
