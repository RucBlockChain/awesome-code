package lee.adds;

import lee.pkg20210922.ListNode;
import org.openjdk.nashorn.api.tree.Tree;

import java.lang.annotation.Native;
import java.util.*;

public class Solution {
    //    https://leetcode-cn.com/problems/3sum/
    public List<List<Integer>> threeSum(int[] nums) {
        // -4 -1 -1 -1 0 1 2
        // -1 -1 2    -1 0 1
        List<List<Integer>> l = new ArrayList<>();
        int n = nums.length;
        if (n < 3) return l;
        Arrays.sort(nums);

        for (int i = 0; i < n; i++) {
            // 0 0 0
            if (nums[i] > 0) break;
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int j = i + 1, k = n - 1;
            while (j < k) {
                if (nums[i] + nums[j] + nums[k] == 0) {
                    l.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    while (j < k && nums[j] == nums[j + 1]) j++;
                    while (j < k && nums[k] == nums[k - 1]) k--;
                    j++;
                    k--;
                } else if (nums[i] + nums[j] + nums[k] > 0) {
                    k--;
                } else {
                    j++;
                }
            }
        }
        return l;
    }

    //    https://leetcode-cn.com/problems/roman-to-integer/
//    s should be a valid roman integer
    //    若存在小的数字在大的数字的左边的情况，根据规则需要减去小的数字。对于这种情况，我们也可以将每个字符视作一个单独的值，若一个数字右侧的数字比它大，则将该数字的符号取反。
    public int romanToInt(String s) {
        char[] arr = s.toCharArray();
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 'I') {
                if (i + 1 < arr.length && arr[i + 1] == 'V') {
                    i++;
                    ans += 4;
                } else if (i + 1 < arr.length && arr[i + 1] == 'X') {
                    i++;
                    ans += 9;
                } else {
                    ans += 1;
                }
            } else if (arr[i] == 'X') {
                if (i + 1 < arr.length && arr[i + 1] == 'L') {
                    i++;
                    ans += 40;
                } else if (i + 1 < arr.length && arr[i + 1] == 'C') {
                    i++;
                    ans += 90;
                } else {
                    ans += 10;
                }
            } else if (arr[i] == 'C') {
                if (i + 1 < arr.length && arr[i + 1] == 'D') {
                    i++;
                    ans += 400;
                } else if (i + 1 < arr.length && arr[i + 1] == 'M') {
                    i++;
                    ans += 900;
                } else {
                    ans += 100;
                }
            } else if (arr[i] == 'V') {
                ans += 5;
            } else if (arr[i] == 'L') {
                ans += 50;
            } else if (arr[i] == 'D') {
                ans += 500;
            } else if (arr[i] == 'M') {
                ans += 1000;
            }

        }
        return ans;
    }

    // you need to treat n as an unsigned value
//    https://leetcode-cn.com/problems/number-of-1-bits/
    public int hammingWeight(int n) {
        /**
         * A constant holding the maximum value an {@code int} can
         * have, 2<sup>31</sup>-1.
         */
//        @Native public static final int   MAX_VALUE = 0x7fffffff;
//        Integer.MAX_VALUE;
        int ans = 0;
        while (n != 0) {
            n -= (n & -n);
            ans++;
        }
        return ans;
    }

    //    https://leetcode-cn.com/problems/shu-zu-zhong-de-ni-xu-dui-lcof/
    public int reversePairs(int[] nums) {
        int len = nums.length;
        if (len < 2) return 0;

        int[] copy = new int[len];

        for (int i = 0; i < len; i++) {
            copy[i] = nums[i];
        }
        int[] temp = new int[len];
        return reversePairs(copy, 0, len - 1, temp);
    }

    private int reversePairs(int[] nums, int left, int right, int[] temp) {
        if (left == right) return 0;
        int mid = left + (right - left) / 2;
        int leftPairs = reversePairs(nums, left, mid, temp);
        int rightPairs = reversePairs(nums, mid + 1, right, temp);
        int crossPairs = mergeAndCount(nums, left, mid, right, temp);

        if (nums[mid] <= nums[mid + 1]) {
            return leftPairs + rightPairs;
        }

        return leftPairs + rightPairs + crossPairs;
    }

    // nums[left...mid] nums[mid+1..right]
    private int mergeAndCount(int[] nums, int left, int mid, int right, int[] temp) {
        for (int i = left; i <= right; i++) {
            temp[i] = nums[i];
        }
        int i = left;
        int j = mid + 1;
        int count = 0;
        for (int k = left; k <= right; k++) {

            if (i == mid + 1) {
                nums[k] = temp[j];
                j++;
            } else if (j == right + 1) {
                nums[k] = temp[i];
                i++;
            } else if (temp[i] <= temp[j]) {
                nums[k] = temp[i];
                i++;
            } else {
                nums[k] = temp[j];
                j++;
                count += (mid - i + 1);
            }
        }
        return count;
    }

    //    https://leetcode-cn.com/problems/er-cha-shu-de-zui-jin-gong-gong-zu-xian-lcof/
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) return root;
        TreeNode l = lowestCommonAncestor(root.left, p, q);
        TreeNode r = lowestCommonAncestor(root.right, p, q);
        if (l == null) return r;
        if (r == null) return l;
        return root;
    }

    //    https://leetcode.cn/problems/shu-de-zi-jie-gou-lcof/
    boolean res = false;

    public boolean isSubStructure(TreeNode A, TreeNode B) {
        if (A == B) return true;
        if (A == null || B == null) return false;
        dfs(A, B); // dfs 的时候 A B已经不是null
        return res;
    }

    public void dfs(TreeNode a, TreeNode b) {
        if (res) return;
        if (a == null) return;
        if (check(a, b)) res = true;
        dfs(a.left, b);
        dfs(a.right, b);
    }

    public boolean check(TreeNode a, TreeNode b) {
        if (b == null) return true;
        if (a == null) return false;
        if (a.val != b.val) return false;
        boolean b1 = check(a.left, b.left);
        boolean b2 = check(a.right, b.right);
        return b1 & b2;
    }

    //    https://leetcode.cn/problems/merge-two-sorted-lists/
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode root = new ListNode();
        ListNode node = root;
        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                node.next = list1;
                list1 = list1.next;
            } else {
                node.next = list2;
                list2 = list2.next;
            }
            node = node.next;
        }
        node.next = (list1 == null) ? list2 : list1;

        return root.next;
    }

    public ListNode mergeTwoLists1(ListNode list1, ListNode list2) {
        if (list1 == null) return list2;
        if (list2 == null) return list1;
        if (list1.val < list2.val) {
            list1.next = mergeTwoLists1(list1.next, list2);
            return list1;
        } else {
            list2.next = mergeTwoLists1(list1, list2.next);
            return list2;
        }
    }

    //    https://leetcode.cn/problems/same-tree/
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null || q == null) {
            if (p == null && q == null) return true;
            return false;
        }
        if (p.val != q.val) return false;
        boolean l = isSameTree(p.left, q.left);
        boolean r = isSameTree(p.right, q.right);
        return l & r;
    }

    //    https://leetcode.cn/problems/single-number/
    public int singleNumber(int[] nums) {
        int ans = 0;
        for (int num : nums) {
            ans ^= num;
        }
        return ans;
    }

    //    https://leetcode.cn/problems/remove-duplicates-from-sorted-list/
    public ListNode deleteDuplicates(ListNode head) {
        ListNode node = head;
        while (node != null) {
            ListNode next = node.next;
            while (next != null && node.val == next.val) {
                next = next.next;
            }
            node.next = next;
            node = node.next;
        }
        return head;
    }

    List<Integer> l = new ArrayList<>();

    //    https://leetcode.cn/problems/binary-tree-preorder-traversal/
    public List<Integer> preorderTraversal(TreeNode root) {
        dfs(root);
        return l;
    }

    void dfs(TreeNode node) {
        if (node == null) return;
        l.add(node.val);
        dfs(node.left);
        dfs(node.right);
    }

    //    https://leetcode.cn/problems/reorder-list/
    public void reorderList(ListNode head) {
        if (head == null) {
            return;
        }
        ListNode mid = middleNode(head);
        ListNode l1 = head;
        ListNode l2 = mid.next;
        mid.next = null;
        l2 = reverseList(l2);
        mergeList(l1, l2);
    }

    //    https://leetcode.cn/problems/reverse-linked-list/
    public ListNode reverseList(ListNode head) {
        ListNode result = new ListNode();
        ListNode cur = head;
        while (cur != null) {
            ListNode tmp = cur;
            cur = cur.next;
            tmp.next = result.next;
            result.next = tmp;
        }
        return result.next;
    }

    public ListNode middleNode(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    public void mergeList(ListNode l1, ListNode l2) {

        while (l1 != null && l2 != null) {
            ListNode l1_tmp = l1.next;
            ListNode l2_tmp = l2.next;

            l1.next = l2;
            l1 = l1_tmp;

            l2.next = l1;
            l2 = l2_tmp;
        }
    }

    public void reorderList1(ListNode head) {
        if (head == null) {
            return;
        }
        List<ListNode> list = new ArrayList<ListNode>();
        ListNode node = head;
        while (node != null) {
            list.add(node);
            node = node.next;
        }
        int i = 0, j = list.size() - 1;
        while (i < j) {
            list.get(i).next = list.get(j);
            i++;
            if (i == j) {
                break;
            }
            list.get(j).next = list.get(i);
            j--;
        }
        list.get(i).next = null;
    }

    //    https://leetcode.cn/problems/linked-list-cycle/
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) return false;
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null) {
            if (slow == fast) return true;
            fast = fast.next;
            slow = slow.next;
            if (fast != null) fast = fast.next;
        }
        return false;
    }

    //    https://leetcode.cn/problems/merge-sorted-array/
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] tmp = new int[m + n];

        for (int i = 0, j = 0, idx = 0; i < m || j < n; idx++) {
            if (i == m) {
                tmp[idx] = nums2[j++];
            } else if (j == n) {
                tmp[idx] = nums1[i++];
            } else if (nums1[i] < nums2[j]) {
                tmp[idx] = nums1[i++];
            } else {
                tmp[idx] = nums2[j++];
            }
        }
        System.arraycopy(tmp, 0, nums1, 0, m + n);
    }

    //    https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/
    public int strStr(String haystack, String needle) {
        return haystack.indexOf(needle);
    }

    List<Integer> l1 = new ArrayList<>();

    //    https://leetcode.cn/problems/binary-tree-postorder-traversal/
    public List<Integer> postorderTraversal(TreeNode root) {
        dfs1(root);
        return l;
    }

    //    https://leetcode.cn/problems/binary-tree-level-order-traversal/
    public List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new ArrayDeque<>();
        List<List<Integer>> ans = new ArrayList<>();
        if (root == null) return ans;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int len = queue.size();
            List<Integer> l = new ArrayList<>();
            for (int i = 0; i < len; i++) {
                TreeNode node = queue.poll();
                l.add(node.val);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            ans.add(l);
        }
        return ans;
    }

    void dfs1(TreeNode node) {
        if (node == null) return;
        dfs1(node.left);
        dfs1(node.right);
        l.add(node.val);
    }

    //    https://leetcode.cn/problems/subtract-the-product-and-sum-of-digits-of-an-integer/description/
    public int subtractProductAndSum(int n) {
        int a = 1;
        int b = 0;
        while (n > 0) {
            a *= (n % 10);
            b += (n % 10);
            n /= 10;
        }
        return a - b;
    }

    //    https://leetcode.cn/problems/reverse-string/
    public void reverseString(char[] s) {
        int len = s.length;
        int i = 0, j = len - 1;
        while (i < j) {
            char tmp = s[i];
            s[i] = s[j];
            s[j] = tmp;
            i++;
            j--;
        }
    }

    public static void main(String[] args) {
        var app = new Solution();
        int[] arr = new int[]{-1, 0, 1, 2, -1, -4};
//        var ans = app.threeSum(arr);
        var ans = app.romanToInt("IV");
        System.out.println(ans);
    }

    //    https://blog.csdn.net/m0_50043893/article/details/126543373
    public static String getMaxLessNum(int[] nums, int N) {
        Arrays.sort(nums);
        String maxBelow = getMaxBelowN(nums, N);
        StringBuilder sb = new StringBuilder();
        boolean flag = false;
        for (int i = 0; i < maxBelow.length(); i++) {
            /*
            每位都找小于等于该位最大的数，如果maxBelow这个位置的数大于等于nums的最大值，就用最大值
            否则去数组找最接近的
            如果当前从数组中找的数字小于maxBelow对应这位的数字，此后选择的数字都可以是数组中的最大值
            例如2513和{2,4,6,8}
            选到第二位4之后，4比5小，此后就可以都选择8，组成2488
             */
            char c = maxBelow.charAt(i);
            if (flag) {
                sb.append(nums[nums.length - 1]);
            } else {
                //拿到应该选择数字的下标
                int index = getIndex(nums, maxBelow, i);
                sb.append(nums[index]);
                //如果选择的数字比当前这位小，此后就都选最大值
                if (nums[index] < c - '0') flag = true;
            }
        }
        return sb.toString();
    }

    /*
    能否拼出N长度的数？还是只能拼出N长度-1的数
    怎么判断能拼出来？
    判断str的第一位与nums[0]的大小关系
     */
    public static String getMaxBelowN(int[] nums, int N) {
        String str = String.valueOf(N);
        int min = nums[0];
        boolean flag = check(str, min);
        int num = flag ? (N - 1) : (int) Math.pow(10, str.length() - 1) - 1;
        return String.valueOf(num);
    }

    //检查能否用数组拼出相同长度的数字字符串
    public static boolean check(String str, int minValue) {
        if (str.equals("")) return true;
        char c = str.charAt(0);
        if (c - '0' > minValue) {
            return true;
        } else if (c - '0' < minValue) {
            return false;
        } else {
            return check(str.substring(1), minValue);
        }
    }

    public static int getIndex(int[] nums, String maxBelow, int index) {
        int curNum = maxBelow.charAt(index) - '0';
        if (index < maxBelow.length() - 1) {
            int nextNum = maxBelow.charAt(index + 1) - '0';
            //下一位不能小于等于nums[0]，否则就要选小于curNum的数
            if (nextNum <= nums[0]) {
                curNum -= 1;
            }
        }

        //curNum处理完成后，找到小于等于curNum的数
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == curNum) {
                left = mid + 1;
            } else if (nums[mid] < curNum) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return right;
    }

    //    https://leetcode.cn/problems/intersection-of-two-linked-lists/
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) return null;
        ListNode pA = headA;
        ListNode pB = headB;
        while (pA != pB) {
            pA = pA == null ? headB : pA.next;
            pB = pB == null ? headA : pB.next;
        }
        return pA;
    }

    //    https://leetcode.cn/problems/non-overlapping-intervals/description/
    public int eraseOverlapIntervals(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        Arrays.sort(intervals, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[1] - o2[1];
            }
        });

        int n = intervals.length;
        int right = intervals[0][1];
        int ans = 1;
        for (int i = 1; i < n; ++i) {
            if (intervals[i][0] >= right) {
                ++ans;
                right = intervals[i][1];
            }
        }
        return n - ans;
    }

}
