package lee.pkg20230819;

public class Solution {
    //    https://leetcode.cn/problems/add-two-integers/
    public int sum(int num1, int num2) {
        return num1 + num2;
    }
}
