package lee.pkg20221129;

public class Solution {
    //    https://leetcode.cn/problems/minimum-changes-to-make-alternating-binary-string/
    public int minOperations(String s) {
        int cnt = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c != (char) ('0' + i % 2)) {
                cnt++;
            }
        }
        return Math.min(cnt, s.length() - cnt);
    }
}