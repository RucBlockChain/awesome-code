package lee.pkg20230805;

import lee.pkg20210922.ListNode;

public class Solution {
    //    https://leetcode.cn/problems/merge-two-sorted-lists/description/
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) return l2;
        else if (l2 == null) return l1;
        else if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }
}
