

| A    | [`abs()`](https://docs.python.org/zh-cn/3/library/functions.html#abs) | [`aiter()`](https://docs.python.org/zh-cn/3/library/functions.html#aiter) | [`all()`](https://docs.python.org/zh-cn/3/library/functions.html#all) | [`any()`](https://docs.python.org/zh-cn/3/library/functions.html#any) | [`anext()`](https://docs.python.org/zh-cn/3/library/functions.html#anext) | [`ascii()`](https://docs.python.org/zh-cn/3/library/functions.html#ascii) |                                                              |                                                              |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| B    | [`bin()`](https://docs.python.org/zh-cn/3/library/functions.html#bin) | [`bool()`](https://docs.python.org/zh-cn/3/library/functions.html#bool) | [`breakpoint()`](https://docs.python.org/zh-cn/3/library/functions.html#breakpoint) | [`bytearray()`](https://docs.python.org/zh-cn/3/library/functions.html#func-bytearray) | [`bytes()`](https://docs.python.org/zh-cn/3/library/functions.html#func-bytes) |                                                              |                                                              |                                                              |
| C    | [`callable()`](https://docs.python.org/zh-cn/3/library/functions.html#callable) | [`chr()`](https://docs.python.org/zh-cn/3/library/functions.html#chr) | [`classmethod()`](https://docs.python.org/zh-cn/3/library/functions.html#classmethod) | [`complex()`](https://docs.python.org/zh-cn/3/library/functions.html#complex) |                                                              |                                                              |                                                              |                                                              |
| D    | [`delattr()`](https://docs.python.org/zh-cn/3/library/functions.html#delattr) | [`dict()`](https://docs.python.org/zh-cn/3/library/functions.html#func-dict) | [`dir()`](https://docs.python.org/zh-cn/3/library/functions.html#dir) | [`divmod()`](https://docs.python.org/zh-cn/3/library/functions.html#divmod) |                                                              |                                                              |                                                              |                                                              |
| E    | [`enumerate()`](https://docs.python.org/zh-cn/3/library/functions.html#enumerate) | [`eval()`](https://docs.python.org/zh-cn/3/library/functions.html#eval) | [`exec()`](https://docs.python.org/zh-cn/3/library/functions.html#exec) |                                                              |                                                              |                                                              |                                                              |                                                              |
| F    | [`filter()`](https://docs.python.org/zh-cn/3/library/functions.html#filter) | [`float()`](https://docs.python.org/zh-cn/3/library/functions.html#float) | [`format()`](https://docs.python.org/zh-cn/3/library/functions.html#format) | [`frozenset()`](https://docs.python.org/zh-cn/3/library/functions.html#func-frozenset) |                                                              |                                                              |                                                              |                                                              |
| G    | [`getattr()`](https://docs.python.org/zh-cn/3/library/functions.html#getattr) | [`globals()`](https://docs.python.org/zh-cn/3/library/functions.html#globals) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |
| H    | [`hasattr()`](https://docs.python.org/zh-cn/3/library/functions.html#hasattr) | [`hash()`](https://docs.python.org/zh-cn/3/library/functions.html#hash) | [`help()`](https://docs.python.org/zh-cn/3/library/functions.html#help) | [`hex()`](https://docs.python.org/zh-cn/3/library/functions.html#hex) |                                                              |                                                              |                                                              |                                                              |
| I    | [`id()`](https://docs.python.org/zh-cn/3/library/functions.html#id) | [`input()`](https://docs.python.org/zh-cn/3/library/functions.html#input) | [`int()`](https://docs.python.org/zh-cn/3/library/functions.html#int) | [`isinstance()`](https://docs.python.org/zh-cn/3/library/functions.html#isinstance) | [`issubclass()`](https://docs.python.org/zh-cn/3/library/functions.html#issubclass) | [`iter()`](https://docs.python.org/zh-cn/3/library/functions.html#iter) |                                                              |                                                              |
| L    | [`len()`](https://docs.python.org/zh-cn/3/library/functions.html#len) | [`list()`](https://docs.python.org/zh-cn/3/library/functions.html#func-list) | [`locals()`](https://docs.python.org/zh-cn/3/library/functions.html#locals) |                                                              |                                                              |                                                              |                                                              |                                                              |
| M    | [`map()`](https://docs.python.org/zh-cn/3/library/functions.html#map) | [`max()`](https://docs.python.org/zh-cn/3/library/functions.html#max) | [`memoryview()`](https://docs.python.org/zh-cn/3/library/functions.html#func-memoryview) | [`min()`](https://docs.python.org/zh-cn/3/library/functions.html#min) |                                                              |                                                              |                                                              |                                                              |
| N    | [`next()`](https://docs.python.org/zh-cn/3/library/functions.html#next) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |
| O    | [`object()`](https://docs.python.org/zh-cn/3/library/functions.html#object) | [`oct()`](https://docs.python.org/zh-cn/3/library/functions.html#oct) | [`open()`](https://docs.python.org/zh-cn/3/library/functions.html#open) | [`ord()`](https://docs.python.org/zh-cn/3/library/functions.html#ord) |                                                              |                                                              |                                                              |                                                              |
| P    | [`pow()`](https://docs.python.org/zh-cn/3/library/functions.html#pow) | [`print()`](https://docs.python.org/zh-cn/3/library/functions.html#print) | [`property()`](https://docs.python.org/zh-cn/3/library/functions.html#property) |                                                              |                                                              |                                                              |                                                              |                                                              |
| R    | [`range()`](https://docs.python.org/zh-cn/3/library/functions.html#func-range) | [`repr()`](https://docs.python.org/zh-cn/3/library/functions.html#repr) | [`reversed()`](https://docs.python.org/zh-cn/3/library/functions.html#reversed) | [`round()`](https://docs.python.org/zh-cn/3/library/functions.html#round) |                                                              |                                                              |                                                              |                                                              |
| S    | [`set()`](https://docs.python.org/zh-cn/3/library/functions.html#func-set) | [`setattr()`](https://docs.python.org/zh-cn/3/library/functions.html#setattr) | [`slice()`](https://docs.python.org/zh-cn/3/library/functions.html#slice) | [`sorted()`](https://docs.python.org/zh-cn/3/library/functions.html#sorted) | [`staticmethod()`](https://docs.python.org/zh-cn/3/library/functions.html#staticmethod) | [`str()`](https://docs.python.org/zh-cn/3/library/functions.html#func-str) | [`sum()`](https://docs.python.org/zh-cn/3/library/functions.html#sum) | [`super()`](https://docs.python.org/zh-cn/3/library/functions.html#super) |
| T    | [`tuple()`](https://docs.python.org/zh-cn/3/library/functions.html#func-tuple) | [`type()`](https://docs.python.org/zh-cn/3/library/functions.html#type) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |
| V    | [`vars()`](https://docs.python.org/zh-cn/3/library/functions.html#vars) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |
| Z    | [`zip()`](https://docs.python.org/zh-cn/3/library/functions.html#zip) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |
| _    | [`__import__()`](https://docs.python.org/zh-cn/3/library/functions.html#import__) |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |                                                              |

> # 内置常量
>
> **False**
>
> **True**
>
> **None**
>
> **NotImplemented**
>
> **Ellipsis**
>
> **__debug__**

### 内置函数

###  a

``` python
abs() #返回一个数的绝对值。 
aiter()
all() #all() 函数用于判断给定的可迭代参数 iterable 中的所有元素是否都为 TRUE，如果是返回 True，否则返回 False。元素除了是 0、空、None、False 外都算 True。
any() #any() 函数用于判断给定的可迭代参数 iterable 是否全部为 False，则返回 False，如果有一个为True，则返回 True。
anext()
ascii()
```

#### b

``` python
bin() # bin(x) 将整数转变为以“0b”前缀的二进制字符串。format() for replace
bool() # 返回布尔值，True 或 False。
breakpoint(*args, **kws) #此函数会在调用时将你陷入调试器中。
class bytearray([source[, encoding[, errors]]]) #返回一个新的 bytes 数组。
class bytes([source[, encoding[, errors]]]) #返回一个新的“bytes”对象，这是一个不可变序列，包含范围为 0 <= x < 256 的整数。
```

### c

```python
```

### e

```python
enumerate(iterable, start=0) #返回一个枚举对象。iterable 必须是一个序列
eval(expression[, globals[, locals]]) # 用来执行一个字符串表达式，并返回表达式的值。
exec("print('hello world exec')")
```



#### v

``` python
vars() # 返回模块、类、实例或任何其它具有 __dict__ 属性的对象的 __dict__ 属性。
```

### _

``` python
__import__() # 函数用于动态加载类和函数 。
```

