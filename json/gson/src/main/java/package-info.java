//java.lang.Object
//        com.google.gson.JsonElement
//        Direct Known Subclasses:
//        JsonArray, JsonNull, JsonObject, JsonPrimitive

//Gson GsonBuilder


// https://www.javadoc.io/doc/com.google.code.gson/gson/latest/com.google.gson/module-summary.html


//FieldAttributes
//        A data object that stores attributes of a field.
//        Gson
//        This is the main class for using Gson.
//        GsonBuilder
//        Use this builder to construct a Gson instance when you need to set configuration options other than the default.
//        JsonArray
//        A class representing an array type in Json.
//        JsonElement
//        A class representing an element of Json.
//        JsonNull
//        A class representing a Json null value.
//        JsonObject
//        A class representing an object type in Json.
//        JsonParser
//        A parser to parse Json into a parse tree of JsonElements
//        JsonPrimitive
//        A class representing a Json primitive value.
//        JsonStreamParser
//        A streaming parser that allows reading of multiple JsonElements from the specified reader asynchronously.
//        TypeAdapter<T>
//        Converts Java objects to and from JSON.


//ExclusionStrategy
//        A strategy (or policy) definition that is used to decide whether or not a field or top-level class should be serialized or deserialized as part of the JSON output/input.
//        FieldNamingStrategy
//        A mechanism for providing custom field naming in Gson.
//        InstanceCreator<T>
//        This interface is implemented to create instances of a class that does not define a no-args constructor.
//        JsonDeserializationContext
//        Context for deserialization that is passed to a custom deserializer during invocation of its JsonDeserializer.deserialize(JsonElement, Type, JsonDeserializationContext) method.
//        JsonDeserializer<T>
//        Interface representing a custom deserializer for Json.
//        JsonSerializationContext
//        Context for serialization that is passed to a custom serializer during invocation of its JsonSerializer.serialize(Object, Type, JsonSerializationContext) method.
//        JsonSerializer<T>
//        Interface representing a custom serializer for Json.
//        TypeAdapterFactory
//        Creates type adapters for set of related types.


//FieldNamingPolicy
//        An enumeration that defines a few standard naming conventions for JSON field names.
//        LongSerializationPolicy
//        Defines the expected format for a long or Long type when its serialized.