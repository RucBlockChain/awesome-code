package org.peter;

import io.smallrye.mutiny.Uni;

// 按两次 Shift 打开“随处搜索”对话框并输入 `show whitespaces`，
// 然后按 Enter 键。现在，您可以在代码中看到空格字符。
public class Main {
    public static void main(String[] args) {
        Uni.createFrom().item("hello")
                .onItem().transform(item -> item + " mutiny")
                .onItem().transform(String::toUpperCase)
                .subscribe().with(item -> System.out.println(">> " + item));


        Uni<String> uni = Uni.createFrom().item("hello");
        uni = uni.onItem().transform(item -> item + " mutiny");
        uni = uni.onItem().transform(String::toUpperCase);
        uni.subscribe().with(item -> System.out.println(">> " + item));


    }
}